MLocate
=======

Locate devices using Wi-Fi signals, and cluster devices using Machine Learning to determine unique users in a given space.

Research and Related Work
-------------------------

- **ArrayTrack: Fine-Grained Indoor Location System**:
  https://www.usenix.org/system/files/conference/nsdi13/nsdi13-final51.pdf

- **Waitz**: https://ucsdwaitz.com/

- **SpotFi: Decimeter Level Localization Using WiFi**:
  https://web.stanford.edu/~skatti/pubs/sigcomm15-spotfi.pdf

- **Accurate, Low-Energy Trajectory Mapping for Mobile Devices**:
  http://db.csail.mit.edu/pubs/ctrack-cr.pdf

Installation
------------

::

    pip install --user -r requirements.txt
