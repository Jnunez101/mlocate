const device_list = $('#device_list');

let state = {};

function enumerate(iterable) {
  const list = [];
  for (let i = 0;i < iterable.length;i++) {
    list.push([i, iterable[i]]);
  }
  return list;
}

function getState(callback) {
  $.getJSON('/retrieve_data',
    (data, status) => {
      if (status !== 'success') {
        // TODO handle this nicely
        console.log('Failed to retrieve section state.');
      }
      state = data;
      callback();
    }).fail(() => {
    // TODO handle this nicely
    console.log('Failed to retrieve section state.');
  });
}

function createCircle(x, y, fill) {
  // Create a Shape DisplayObject.
  const circle = new createjs.Shape();
  circle.graphics.beginFill(fill).drawCircle(0, 0, 10);

  // Set position of Shape instance.
  circle.x = x;
  circle.y = 768 - y;

  return circle;
}

// Create a stage by getting a reference to the canvas
const stage = new createjs.Stage('live-view');
let active_device = null;

const background = new Image();
background.src = '/resources/images/room_bg.png';

function render() {
  console.log(state);

  // Draw the background
  var bitmap = new createjs.Bitmap(background);
  stage.addChild(bitmap);

  for (const location of state.nodes) {
    // Add Shape instance to stage display list.
    stage.addChild(createCircle(location[0], location[1], 'black'));
  }

  // Device List
  device_list.empty();
  for (const [i, [mac, location]] of enumerate(Object.entries(state.devices))) {
    console.log(mac, location);

    device_list.append(`
      <li id="device-listing-${i}"
          class="device-listing list-group-item ${active_device === i ? 'active' : ''}">
        ${mac}
      </li>
    `);
    $(`#device-listing-${i}`).on('click', () => {
      console.log(mac);
      $('.device-listing').removeClass('active');
      $(`#device-listing-${i}`).addClass('active');
      active_device = i;
    });

    // Add Shape instance to stage display list.
    stage.addChild(createCircle(location[0], location[1],
      active_device === i ? 'red' : 'green'));
  }

  // Update stage will render next frame
  stage.update();
}

setInterval(() => getState(render), 1000);
