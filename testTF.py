import tensorflow as tf
import sys, subprocess, os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

tf.enable_eager_execution()

print(tf.reduce_sum(tf.random_normal([1000, 1000])))
